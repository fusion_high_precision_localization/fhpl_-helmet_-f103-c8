/*
 * This file is part of the EasyLogger Library.
 *
 * Copyright (c) 2015, Armink, <armink.ztl@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * 'Software'), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Function: Portable interface for each platform.
 * Created on: 2015-04-28
 */
 
#include <elog.h>
#include <stdio.h>

#include "main.h"
#include "cmsis_os.h"
#include "SEGGER_RTT.h"

static uint32_t tick_freq;
static osMutexId elog_mutex;
const osMutexAttr_t elog_mutex_attr = {
		"Elog Mutex",
		osMutexRecursive,
		NULL,
		0
};

/**
 * EasyLogger port initialize
 *
 * @return result
 */
ElogErrCode elog_port_init(void) {
    ElogErrCode result = ELOG_NO_ERR;
    elog_mutex = osMutexNew(&elog_mutex_attr);
    assert_param(elog_mutex!=NULL);
    tick_freq = osKernelGetSysTimerFreq();
    return result;
}

/**
 * EasyLogger port deinitialize
 *
 */
void elog_port_deinit(void) {
    osStatus st;
	st = osMutexDelete(elog_mutex);
	assert_param(st==osOK);
}

/**
 * output log port interface
 *
 * @param log output of log
 * @param size log size
 */
void elog_port_output(const char *log, size_t size) {
	assert_param(log!=NULL);
	assert_param(size!=0);
    SEGGER_RTT_Write(0, log, size);
}

/**
 * output lock
 */
void elog_port_output_lock(void) {
    osStatus st;
	st = osMutexAcquire(elog_mutex, osWaitForever);
	assert_param(st==osOK);
}

/**
 * output unlock
 */
void elog_port_output_unlock(void) {
    osStatus st;
    st = osMutexRelease(elog_mutex);
    assert_param(st==osOK);
}

/**
 * get current time interface
 *
 * @return current time
 */
const char *elog_port_get_time(void) {
    static char cur_system_time[16]="";
    snprintf(cur_system_time,16,"%010lu ms",osKernelSysTick()/tick_freq*1000);
    return cur_system_time;
}

/**
 * get current process name interface
 *
 * @return current process name
 */
const char *elog_port_get_p_info(void) {
    return "";
}

/**
 * get current thread name interface
 *
 * @return current thread name
 */
const char *elog_port_get_t_info(void) {
	const char* name = osThreadGetName(osThreadGetId());
	assert_param(name!=NULL);
	return name;
}
